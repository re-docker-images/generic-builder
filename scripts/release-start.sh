#!/usr/bin/env bash
set -o pipefail

if [[ -z "$1" ]]; then
  current_version=$(git ls-remote --tags -q)
  new_version=$(increment-version.sh "$current_version")
else
  new_version=$1
fi

# Configure git user
git config user.email "bot@kirdia.me"
git config user.name "CI Bot"
git config push.default simple

# Store any local changes
git stash

# Initialised git flow in this repository
git flow init -d || exit 1

# Apply any stashed changes
git stash pop

# Begin a new release
git flow release start "$new_version" || exit 1
