SHELL := /bin/bash

# ============================================================================


# ============================================================================

IMAGE_NAME := generic-builder
BUILD_NAMESPACE ?= registry.gitlab.com/re-docker-images

BUILD_IMAGE := $(BUILD_NAMESPACE)/$(IMAGE_NAME)
export BUILD_IMAGE

BASE_IMAGE_NAME ?= php
BASE_IMAGE_VERSION ?= 7.2
export BASE_IMAGE_NAME

BASE_IMAGE := $(BASE_IMAGE_NAME):$(BASE_IMAGE_VERSION)
export BASE_IMAGE

MAINTAINER_NAME 	?= Kyriakos Diamantis
MAINTAINER_EMAIL 	?= diamantis.kyr@gmail.com

AUTHOR := $(MAINTAINER_NAME) <$(MAINTAINER_EMAIL)>
export AUTHOR

# ============================================================================

SED_MATCH ?= [^a-zA-Z0-9._-]

ifeq ($(CI_SERVER_NAME),'Gitlab')
# Configure build variables based on CircleCI environment vars
BUILD_NUM = build-$(CI_JOB_ID)
BRANCH_NAME ?= $(shell sed 's/$(SED_MATCH)/-/g' <<< "$(CI_COMMIT_REF_NAME)")
BUILD_TAG ?= $(shell sed 's/$(SED_MATCH)/-/g' <<< "$(CI_COMMIT_TAG)")
else
# Not in CircleCI environment, try to set sane defaults
BUILD_NUM = build-$(shell uname -n | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9._-]/-/g')
BRANCH_NAME ?= $(shell git rev-parse --abbrev-ref HEAD | sed 's/$(SED_MATCH)/-/g')
BUILD_TAG ?= $(shell git tag -l --points-at HEAD | tail -n1 | sed 's/$(SED_MATCH)/-/g')
endif

# If BUILD_TAG is blank there's no tag on this commit
ifeq ($(strip $(BUILD_TAG)),)
# Default to branch name
BUILD_TAG := $(BRANCH_NAME)
else
# Consider this the new :latest image
# FIXME: implement build tests before tagging with :latest
PUSH_LATEST := true
endif

REVISION_TAG = $(shell git rev-parse --short HEAD)

# ============================================================================

# Check necessary commands exist

#CIRCLECI := $(shell command -v circleci 2> /dev/null)
DOCKER := $(shell command -v docker 2> /dev/null)
ENVSUBST := $(shell command -v envsubst 2> /dev/null)
SHELLCHECK := $(shell command -v shellcheck 2> /dev/null)
YAMLLINT := $(shell command -v yamllint 2> /dev/null)

# ============================================================================

all: init clean build test push

init:
	@chmod 755 .githooks/*
	@find .git/hooks -type l -exec rm {} \;
	@find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

clean:
	find . -type f -name 'Dockerfile' -exec rm {} \;

lint: lint-sh lint-yaml lint-docker

lint-yaml:
ifndef YAMLLINT
	$(error "yamllint is not installed: https://github.com/adrienverge/yamllint")
endif
	@find . -type f -name '*.yml' | xargs yamllint

lint-docker: Dockerfile
ifndef DOCKER
	$(error "docker is not installed: https://docs.docker.com/install/")
endif
	docker run --rm -i hadolint/hadolint < Dockerfile

lint-sh:
ifndef SHELLCHECK
	$(error "shellcheck is not installed: https://github.com/koalaman/shellcheck")
endif
	@find .  -type f -name '*.sh' | xargs shellcheck


pull:
	docker pull $(BASE_IMAGE)

Dockerfile:
	envsubst '$${BASE_IMAGE},$${AUTHOR},$${PHP_VERSION}' \
		< Dockerfile.in > $@ ; \

build:
ifndef DOCKER
$(error "docker is not installed: https://docs.docker.com/install/")
endif
	$(MAKE) -j lint pull
	echo $$BASE_IMAGE_NAME
	echo ${BASE_IMAGE_NAME}
	docker build \
		--tag=$(BUILD_IMAGE):$(BUILD_TAG) \
		--tag=$(BUILD_IMAGE):$(BUILD_NUM) \
		. ;

.PHONY: test
test2:
	docker run --rm -v $$(pwd)/tests:/tmp \
		-e "DOCKER_HOST=$$(DOCKER_HOST)" \
		gcr.io/gcp-runtimes/container-structure-test \
		test --image $(BUILD_IMAGE):$(BUILD_TAG) --config /tmp/builder-tests.yml
test:
	container-structure-test test --image $(BUILD_IMAGE):$(BUILD_TAG) --config ./tests/builder-tests.yml

push: push-tag

push-tag:
ifndef DOCKER
	$(error "docker is not installed: https://docs.docker.com/install/")
endif
	docker push $(BUILD_IMAGE):$(BUILD_TAG)
	docker push $(BUILD_IMAGE):$(BUILD_NUM)

push-latest:
ifndef DOCKER
	$(error "docker is not installed: https://docs.docker.com/install/")
endif
	if [[ "$(PUSH_LATEST)" = "true" ]]; then { \
		docker tag $(BUILD_IMAGE):$(REVISION_TAG) $(BUILD_IMAGE):latest; \
		docker push $(BUILD_IMAGE):latest; \
	}	else { \
		echo "Not tagged.. skipping latest"; \
	} fi
